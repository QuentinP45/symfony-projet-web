<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505181856 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE constitution (id INT AUTO_INCREMENT NOT NULL, modele_chaussette_id INT NOT NULL, matiere_chaussette_id INT NOT NULL, taille_chaussette_id INT NOT NULL, provenance LONGTEXT NOT NULL, prix_unitaire DOUBLE PRECISION NOT NULL, qte_disponible INT NOT NULL, INDEX IDX_9DB49BDD6CC5ACFF (modele_chaussette_id), INDEX IDX_9DB49BDDB6618E90 (matiere_chaussette_id), INDEX IDX_9DB49BDD72DFB78E (taille_chaussette_id), UNIQUE INDEX UNIQ_9DB49BDD6CC5ACFFB6618E9072DFB78E (modele_chaussette_id, matiere_chaussette_id, taille_chaussette_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE constitution ADD CONSTRAINT FK_9DB49BDD6CC5ACFF FOREIGN KEY (modele_chaussette_id) REFERENCES modele_chaussette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE constitution ADD CONSTRAINT FK_9DB49BDDB6618E90 FOREIGN KEY (matiere_chaussette_id) REFERENCES matiere_chaussette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE constitution ADD CONSTRAINT FK_9DB49BDD72DFB78E FOREIGN KEY (taille_chaussette_id) REFERENCES taille_chaussette (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE constitution');
    }
}
