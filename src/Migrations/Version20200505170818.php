<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505170818 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE matiere_chaussette (id INT AUTO_INCREMENT NOT NULL, nom_matiere VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_54772B56659B5C82 (nom_matiere), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modele_chaussette (id INT AUTO_INCREMENT NOT NULL, nom_modele VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_75E8B70990CD1A0F (nom_modele), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taille_chaussette (id INT AUTO_INCREMENT NOT NULL, taille_chaussette VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_71D5D14E71D5D14E (taille_chaussette), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE matiere_chaussette');
        $this->addSql('DROP TABLE modele_chaussette');
        $this->addSql('DROP TABLE taille_chaussette');
    }
}
