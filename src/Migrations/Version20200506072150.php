<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200506072150 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE choix_utilisateur (id INT AUTO_INCREMENT NOT NULL, constitution_id INT NOT NULL, panier_id INT NOT NULL, qte_voulue INT NOT NULL, prix_total DOUBLE PRECISION NOT NULL, INDEX IDX_4F7CA123BDA9478A (constitution_id), INDEX IDX_4F7CA123F77D927C (panier_id), UNIQUE INDEX UNIQ_4F7CA123BDA9478AF77D927C (constitution_id, panier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE panier (id INT AUTO_INCREMENT NOT NULL, date_panier DATETIME NOT NULL, valeur_panier DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE choix_utilisateur ADD CONSTRAINT FK_4F7CA123BDA9478A FOREIGN KEY (constitution_id) REFERENCES constitution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE choix_utilisateur ADD CONSTRAINT FK_4F7CA123F77D927C FOREIGN KEY (panier_id) REFERENCES panier (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE choix_utilisateur DROP FOREIGN KEY FK_4F7CA123F77D927C');
        $this->addSql('DROP TABLE choix_utilisateur');
        $this->addSql('DROP TABLE panier');
    }
}
