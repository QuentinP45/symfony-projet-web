<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\Constitution;
use App\Entity\Panier;
use App\Entity\ChoixUtilisateur;

/**
 * @Route("/chauss-7")
 */
class PanierController extends AbstractController {
    /**
     * @Route(
     * "/panier",
     * name="panier"
     * )
     */
    public function Panier(SessionInterface $session)
    {
        // récupération des paniers existants
        $paniersSauves = $this->getDoctrine()
        ->getRepository(Panier::class)
        ->findAll();
        ;

        $paniersAAfficher=[];
        $i=0;
        foreach ($paniersSauves as $panierSauve) {
            $paniersAAfficher[$i]['idPanier'] = $panierSauve->getId();
            $paniersAAfficher[$i]['prixPanier'] = $panierSauve->getValeurPanier();

            //boucle sur les éléments d'un panier (choix utilisateur)
            $choixUtilisateurs = $panierSauve->getChoixUtilisateurs()->getValues();
            $y=0;
            foreach ($choixUtilisateurs as $choixUtilisateur) {
                $paniersAAfficher[$i]['detailsPanier'][$y]=[
                    'idChoix' => $choixUtilisateur->getId(),
                    'modele' => $choixUtilisateur->getConstitution()->getNomModele()->getNomModele(),
                    'matiere' => $choixUtilisateur->getConstitution()->getNomMatiere()->getNomMatiere(),
                    'taille' => $choixUtilisateur->getConstitution()->getTailleChaussette()->getTailleChaussette(),
                    'prixUnitaire' => $choixUtilisateur->getConstitution()->getPrixUnitaire(),
                    'qteVoulue' => $choixUtilisateur->getQteVoulue(),
                    'prixTotal' => $choixUtilisateur->getPrixTotal()
                ];
                $y++;
            }
            $i++;
        }
        
        // création/récupération d'une variable de session de nom panier
        $articles = $session->get('panier', []);

        $valTotalePanier=0;
        $panier = [];
        foreach ($articles as $idConstitution => $quantites) {
            $article = $this->getDoctrine()
                ->getRepository(Constitution::class)
                ->getModeleMatiereTaille($idConstitution)
            ;

            $valTotalePanier+=$article['prix_unitaire'] * $quantites['nbAjouts'];

            $panier[]=[
                $article,
                $quantites['nbAjouts']
            ];
        }

        return $this->render('panier/mon_panier.html.twig', [
            // panier d'articles récupérés de la variable de session
            'panier' => $panier,
            'valTotalePanier' => $valTotalePanier,
            'paniersSauves' => $paniersAAfficher
        ]);
    }

    /**
     * @Route(
     * "/ajout-panier/{id}",
     * name="ajout_panier",
     * methods={"GET"}
     * )
     */
    public function AjoutPanier($id, SessionInterface $session)
    {
        // si panier n'existe pas création d'un tableau vide par défaut
        $panier = $session->get('panier', []);
        
        // si constitution non présente dans le panier
        if (!array_key_exists($id,$panier)) {
            $panier[$id]['nbAjouts']=0;

            $qteDisponibleConstitution = $this->getDoctrine()
                ->getRepository(Constitution::class)
                ->getConstitutionQteDisponible($id)['qteDisponible'];

            $panier[$id]['qteDisponible']=$qteDisponibleConstitution;
        }

        // si quantite disponible de la constitution > 0
        if ($panier[$id]['qteDisponible'] > 0) {
            // prise en compte de l'ajout au panier
            $panier[$id]['nbAjouts'] += 1;

            // diminution quantité disponible
            $panier[$id]['qteDisponible'] -= 1;

            // mise à jour du panier en session
            $session->set('panier', $panier);
        }

        return $this->redirectToRoute('accueil');
    }

    /**
     * @Route(
     * "/sauver-panier",
     * name="sauver_panier",
     * methods={"GET"}
     * )
     */
    public function SauverPanier(SessionInterface $session)
    {
        // récupération du panier en session
        $articles = $session->get('panier');
        
        $valTotalePanier=0;

        // création d'un panier
        $panierEntity = new Panier();
        $panierEntity->setDatePanier(new \DateTime);
        $panierEntity->setValeurPanier(0);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($panierEntity);
        $em->flush();

        foreach ($articles as $idConstitution => $quantites) {
            $article = $this->getDoctrine()
            ->getRepository(Constitution::class)
            ->find($idConstitution);
            ;

            $choixUtilisateur = new ChoixUtilisateur();
            $choixUtilisateur->setConstitution($article);
            $choixUtilisateur->setPanier($panierEntity);
            $choixUtilisateur->setQteVoulue($quantites['nbAjouts']);
            $choixUtilisateur->setPrixTotal($article->getPrixUnitaire() * $quantites['nbAjouts']);

            $em->persist($choixUtilisateur);
            
            $valTotalePanier+=$article->getPrixUnitaire() * $quantites['nbAjouts'];
        }

        $em->flush();

        $panierEntity->setValeurPanier($valTotalePanier);

        $em->persist($panierEntity);
        $em->flush();

        $session->set('panier', []);

        // gestion des choix utilisateurs
        return $this->redirectToRoute('panier');
    }

}