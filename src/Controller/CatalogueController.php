<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Constitution;
// use App\Entity\MatiereChaussette;

/**
 * @Route("/chauss-7")
 */
class CatalogueController extends AbstractController {
    /**
     * @Route(
     * "/accueil",
     * name="accueil"
     * )
     */
    public function Accueil()
    {
        $constitutions = $this->getDoctrine()
            ->getRepository(Constitution::class)
            ->getModelesMatieresTailles();
        
        $vueDEnsemble=[];
        foreach($constitutions as $key => $constitution) {
            if ($constitution->qte_disponible != 0) {
                if (!array_key_exists($constitution->nom_modele,$vueDEnsemble)) {
                    $vueDEnsemble[$constitution->nom_modele]=[];
                    // création de l'index modele avec tableau vide en valeur
                    /* ex : $vueDEnsemble=[
                    *           modele=>[]
                    *       ];
                    */
                }
                    
                if (!array_key_exists($constitution->nom_matiere,$vueDEnsemble[$constitution->nom_modele])) {
                    $vueDEnsemble[$constitution->nom_modele][$constitution->nom_matiere]=[];
                    // création de l'index matiere avec tableau vide en valeur
                    /* ex : $vueDEnsemble=[
                    *          modele=>[
                    *             matiere=>[]
                    *          ]
                    *       ];
                    */
                }

                if (!in_array($constitution->taille_chaussette,$vueDEnsemble[$constitution->nom_modele][$constitution->nom_matiere])) {
                    $vueDEnsemble[$constitution->nom_modele][$constitution->nom_matiere][$constitution->taille_chaussette]=$constitution->id;
                    /* ex : $vueDEnsemble=[
                    *           modele=>[
                    *               matiere=>[
                                        taille=>id
                                    ]
                    *           ]
                    *       ];
                    */
                }
            }
        }
        
        return $this->render('catalogue/accueil.html.twig', [
            'vueDEnsemble' => $vueDEnsemble
        ]);
    }
}