<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ModeleChaussette;
use App\Entity\MatiereChaussette;
use App\Entity\TailleChaussette;
use App\Entity\Constitution;
use App\Entity\ChoixUtilisateur;
use App\Entity\Panier;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // modèles de chaussettes
        $modeleChaussette1 = new ModeleChaussette();
        $modeleChaussette1->setNomModele('Simpsons');
        $manager->persist($modeleChaussette1);

        $modeleChaussette2 = new ModeleChaussette();
        $modeleChaussette2->setNomModele('South park');
        $manager->persist($modeleChaussette2);
        
        // matières de chaussettes
        $matiereChaussette1 = new MatiereChaussette();
        $matiereChaussette1->setNomMatiere('Synthétique');
        $manager->persist($matiereChaussette1);

        $matiereChaussette2 = new MatiereChaussette();
        $matiereChaussette2->setNomMatiere('Coton');
        $manager->persist($matiereChaussette2);

        $manager->flush();

        // tailles des chaussettes
        $tailleChaussette1 = new TailleChaussette();
        $tailleChaussette1->setTailleChaussette('S');
        $manager->persist($tailleChaussette1);

        $tailleChaussette2 = new TailleChaussette();
        $tailleChaussette2->setTailleChaussette('M');
        $manager->persist($tailleChaussette2);

        $manager->flush();

        // constitutions
        // md1 mt1 t1
        $constitution1 = new Constitution();
        $constitution1->setNomModele($modeleChaussette1);
        $constitution1->setNomMatiere($matiereChaussette1);
        $constitution1->setTailleChaussette($tailleChaussette1);
        $constitution1->setProvenance("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?");
        $constitution1->setPrixUnitaire(5);
        $constitution1->setQteDisponible(10);

        $manager->persist($constitution1);

        // md1 mt1 t2
        $constitution2 = new Constitution();
        $constitution2->setNomModele($modeleChaussette1);
        $constitution2->setNomMatiere($matiereChaussette1);
        $constitution2->setTailleChaussette($tailleChaussette2);
        $constitution2->setProvenance("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        $constitution2->setPrixUnitaire(7);
        $constitution2->setQteDisponible(14);

        $manager->persist($constitution2);

        // md1 mt2 t1
        $constitution3 = new Constitution();
        $constitution3->setNomModele($modeleChaussette1);
        $constitution3->setNomMatiere($matiereChaussette2);
        $constitution3->setTailleChaussette($tailleChaussette1);
        $constitution3->setProvenance("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?");
        $constitution3->setPrixUnitaire(8);
        $constitution3->setQteDisponible(23);

        $manager->persist($constitution3);

        // md2 mt1 t1
        $constitution4 = new Constitution();
        $constitution4->setNomModele($modeleChaussette2);
        $constitution4->setNomMatiere($matiereChaussette1);
        $constitution4->setTailleChaussette($tailleChaussette1);
        $constitution4->setProvenance("At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.");
        $constitution4->setPrixUnitaire(4);
        $constitution4->setQteDisponible(7);

        $manager->persist($constitution4);

        // md2 mt2 t2
        $constitution5 = new Constitution();
        $constitution5->setNomModele($modeleChaussette1);
        $constitution5->setNomMatiere($matiereChaussette2);
        $constitution5->setTailleChaussette($tailleChaussette2);
        $constitution5->setProvenance("Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.");
        $constitution5->setPrixUnitaire(6);
        $constitution5->setQteDisponible(27);

        $manager->persist($constitution5);

        $manager->flush();

        // paniers
        $panier1 = new Panier();
        $panier1->setDatePanier(new \DateTime());
        $panier1->setValeurPanier(29);
        
        $manager->persist($panier1);
        
        $panier2 = new Panier();
        $panier2->setDatePanier(new \DateTime());
        $panier2->setValeurPanier(44);
        
        $manager->persist($panier2);
        $manager->flush();

        // choixUtilisateur
        $choixUtil1 = new ChoixUtilisateur();
        $choixUtil1->setPanier($panier1);
        $choixUtil1->setConstitution($constitution1);
        $choixUtil1->setQteVoulue(3);
        $choixUtil1->setPrixTotal(15);

        $manager->persist($choixUtil1);

        $choixUtil2 = new ChoixUtilisateur();
        $choixUtil2->setPanier($panier1);
        $choixUtil2->setConstitution($constitution2);
        $choixUtil2->setQteVoulue(2);
        $choixUtil2->setPrixTotal(14);

        $manager->persist($choixUtil2);

        $choixUtil3 = new ChoixUtilisateur();
        $choixUtil3->setPanier($panier2);
        $choixUtil3->setConstitution($constitution3);
        $choixUtil3->setQteVoulue(5);
        $choixUtil3->setPrixTotal(40);

        $manager->persist($choixUtil3);

        $choixUtil4 = new ChoixUtilisateur();
        $choixUtil4->setPanier($panier2);
        $choixUtil4->setConstitution($constitution4);
        $choixUtil4->setQteVoulue(4);
        $choixUtil4->setPrixTotal(4);

        $manager->persist($choixUtil4);
        $manager->flush();
    }
}
