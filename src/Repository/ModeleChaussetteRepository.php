<?php

namespace App\Repository;

use App\Entity\ModeleChaussette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModeleChaussette|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModeleChaussette|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModeleChaussette[]    findAll()
 * @method ModeleChaussette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeleChaussetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModeleChaussette::class);
    }

    // /**
    //  * @return ModeleChaussette[] Returns an array of ModeleChaussette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModeleChaussette
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
