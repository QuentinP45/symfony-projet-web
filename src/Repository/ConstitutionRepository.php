<?php

namespace App\Repository;

use App\Entity\Constitution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Constitution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Constitution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Constitution[]    findAll()
 * @method Constitution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConstitutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Constitution::class);
    }

    // /**
    //  * @return Constitution[] Returns an array of Constitution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Constitution
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getModelesMatieresTailles() {
        // native sql
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT c.id, nom_modele, nom_matiere, taille_chaussette, qte_disponible
            FROM constitution c
            JOIN matiere_chaussette mat
                ON c.matiere_chaussette_id=mat.id
            JOIN modele_chaussette model 
                ON c.modele_chaussette_id=model.id
            JOIN taille_chaussette tai
                ON c.taille_chaussette_id=tai.id
            GROUP BY nom_modele, nom_matiere, taille_chaussette;
        ';
        $stmt = $conn->query($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getModeleMatiereTaille($id) {
        // native sql
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT c.id, nom_modele, nom_matiere, taille_chaussette, qte_disponible, prix_unitaire
            FROM constitution c
            JOIN matiere_chaussette mat
                ON c.matiere_chaussette_id=mat.id
            JOIN modele_chaussette model 
                ON c.modele_chaussette_id=model.id
            JOIN taille_chaussette tai
                ON c.taille_chaussette_id=tai.id
            WHERE c.id=:id
            GROUP BY nom_modele, nom_matiere, taille_chaussette;
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getConstitutionQteDisponible($id) {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :val')
            ->setParameter('val', $id)
            ->select('c.qteDisponible')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
