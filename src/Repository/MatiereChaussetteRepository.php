<?php

namespace App\Repository;

use App\Entity\MatiereChaussette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatiereChaussette|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatiereChaussette|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatiereChaussette[]    findAll()
 * @method MatiereChaussette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatiereChaussetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatiereChaussette::class);
    }

    // /**
    //  * @return MatiereChaussette[] Returns an array of MatiereChaussette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MatiereChaussette
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
