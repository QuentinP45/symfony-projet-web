<?php

namespace App\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConstitutionRepository")
 * @ORM\Table(
 *      name="constitution",
 *      uniqueConstraints={@ORM\UniqueConstraint(columns={"modele_chaussette_id", "matiere_chaussette_id", "taille_chaussette_id"})}
 * )
 */
class Constitution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $provenance;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private $prixUnitaire;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $qteDisponible;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ModeleChaussette")
     * @ORM\JoinColumn(
     *      name="modele_chaussette_id",
     *      referencedColumnName="id",
     *      nullable=false,
     *      onDelete="CASCADE"
     * )
     */
    private $nomModele;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MatiereChaussette")
     * @ORM\JoinColumn(
     *      name="matiere_chaussette_id",
     *      referencedColumnName="id",
     *      nullable=false,
     *      onDelete="CASCADE"
     * )
     */
    private $nomMatiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TailleChaussette")
     * @ORM\JoinColumn(
     *      name="taille_chaussette_id",
     *      referencedColumnName="id",
     *      nullable=false,
     *      onDelete="CASCADE"
     * )
     */
    private $tailleChaussette;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProvenance(): ?string
    {
        return $this->provenance;
    }

    public function setProvenance(string $provenance): self
    {
        $this->provenance = $provenance;

        return $this;
    }

    public function getPrixUnitaire(): ?float
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(float $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getQteDisponible(): ?int
    {
        return $this->qteDisponible;
    }

    public function setQteDisponible(int $qteDisponible): self
    {
        $this->qteDisponible = $qteDisponible;

        return $this;
    }

    public function getNomModele(): ?ModeleChaussette
    {
        return $this->nomModele;
    }

    public function setNomModele(?ModeleChaussette $nomModele): self
    {
        $this->nomModele = $nomModele;

        return $this;
    }

    public function getNomMatiere(): ?MatiereChaussette
    {
        return $this->nomMatiere;
    }

    public function setNomMatiere(?MatiereChaussette $nomMatiere): self
    {
        $this->nomMatiere = $nomMatiere;

        return $this;
    }

    public function getTailleChaussette(): ?TailleChaussette
    {
        return $this->tailleChaussette;
    }

    public function setTailleChaussette(?TailleChaussette $tailleChaussette): self
    {
        $this->tailleChaussette = $tailleChaussette;

        return $this;
    }
}
