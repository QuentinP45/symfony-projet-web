<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChoixUtilisateurRepository")
 * @ORM\Table(
 *      name="choix_utilisateur",
 *      uniqueConstraints={@ORM\UniqueConstraint(columns={"constitution_id", "panier_id"})}
 * )
 */
class ChoixUtilisateur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $qteVoulue;

    /**
     * @ORM\Column(type="float")
     */
    private $prixTotal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Constitution")
     * @ORM\JoinColumn(
     *      name="constitution_id",
     *      referencedColumnName="id",
     *      nullable=false,
     *      onDelete="CASCADE"
     * )
     */
    private $constitution;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Panier", inversedBy="choixUtilisateurs")
     * @ORM\JoinColumn(
     *      name="panier_id",
     *      referencedColumnName="id",
     *      nullable=false,
     *      onDelete="CASCADE"
     * )
     */
    private $panier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQteVoulue(): ?int
    {
        return $this->qteVoulue;
    }

    public function setQteVoulue(int $qteVoulue): self
    {
        $this->qteVoulue = $qteVoulue;

        return $this;
    }

    public function getPrixTotal(): ?float
    {
        return $this->prixTotal;
    }

    public function setPrixTotal(float $prixTotal): self
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }

    public function getConstitution(): ?constitution
    {
        return $this->constitution;
    }

    public function setConstitution(?constitution $constitution): self
    {
        $this->constitution = $constitution;

        return $this;
    }

    public function getPanier(): ?Panier
    {
        return $this->panier;
    }

    public function setPanier(?Panier $panier): self
    {
        $this->panier = $panier;

        return $this;
    }
}
