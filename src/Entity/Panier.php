<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePanier;

    /**
     * @ORM\Column(type="float")
     */
    private $valeurPanier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChoixUtilisateur", mappedBy="panier", orphanRemoval=true)
     */
    private $choixUtilisateurs;

    public function __construct()
    {
        $this->choixUtilisateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePanier(): ?\DateTimeInterface
    {
        return $this->datePanier;
    }

    public function setDatePanier(\DateTimeInterface $datePanier): self
    {
        $this->datePanier = $datePanier;

        return $this;
    }

    public function getValeurPanier(): ?float
    {
        return $this->valeurPanier;
    }

    public function setValeurPanier(float $valeurPanier): self
    {
        $this->valeurPanier = $valeurPanier;

        return $this;
    }

    /**
     * @return Collection|ChoixUtilisateur[]
     */
    public function getChoixUtilisateurs(): Collection
    {
        return $this->choixUtilisateurs;
    }

    public function addChoixUtilisateur(ChoixUtilisateur $choixUtilisateur): self
    {
        if (!$this->choixUtilisateurs->contains($choixUtilisateur)) {
            $this->choixUtilisateurs[] = $choixUtilisateur;
            $choixUtilisateur->setPanier($this);
        }

        return $this;
    }

    public function removeChoixUtilisateur(ChoixUtilisateur $choixUtilisateur): self
    {
        if ($this->choixUtilisateurs->contains($choixUtilisateur)) {
            $this->choixUtilisateurs->removeElement($choixUtilisateur);
            // set the owning side to null (unless already changed)
            if ($choixUtilisateur->getPanier() === $this) {
                $choixUtilisateur->setPanier(null);
            }
        }

        return $this;
    }
}
