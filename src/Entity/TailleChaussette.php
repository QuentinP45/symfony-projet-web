<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TailleChaussetteRepository")
 */
class TailleChaussette
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $tailleChaussette;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTailleChaussette(): ?string
    {
        return $this->tailleChaussette;
    }

    public function setTailleChaussette(string $tailleChaussette): self
    {
        $this->tailleChaussette = $tailleChaussette;

        return $this;
    }
}
